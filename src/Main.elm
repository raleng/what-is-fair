module Main exposing (..)

-- Press buttons to increment and decrement a counter.
--
-- Read how it works:
--   https://guide.elm-lang.org/architecture/buttons.html
--

import Browser
import Browser.Dom exposing (Element)
import Debug exposing (toString)
import Element exposing (rgb255)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Html exposing (Html)
import Html.Attributes exposing (..)
import List exposing (map5)



-- MAIN


main : Program () Model Msg
main =
    Browser.sandbox { init = init, update = update, view = view }



-- MODEL


type alias Model =
    { expense : Int
    , contributions : List Contribution
    , uid : Int
    }


type alias Contribution =
    { income : Int
    , percentage : Int
    , scaled : Int
    , id : Int
    }


newContribution : Int -> Contribution
newContribution id =
    { id = id, income = 0, percentage = 100, scaled = 0 }


init : Model
init =
    { expense = 0
    , contributions = [ newContribution 0, newContribution 1 ]
    , uid = 2
    }



-- UPDATE


type Msg
    = EditExpense String
    | EditIncome Int String
    | EditPercentage Int String
    | AddContribution
    | DeleteContribution Int


update : Msg -> Model -> Model
update msg model =
    case msg of
        EditExpense newExpense ->
            { model | expense = Maybe.withDefault 0 <| String.toInt newExpense }

        EditIncome id newIncome ->
            { model | contributions = List.map (updateIncome id newIncome) model.contributions }

        EditPercentage id newPercentage ->
            { model | contributions = List.map (updatePercentage id newPercentage) model.contributions }

        AddContribution ->
            { model
                | contributions = model.contributions ++ [ newContribution model.uid ]
                , uid = model.uid + 1
            }

        DeleteContribution id ->
            { model
                | contributions = List.filter (\c -> c.id /= id) model.contributions
            }


updateIncome : Int -> String -> Contribution -> Contribution
updateIncome id income c =
    if c.id == id then
        { c
            | income = Maybe.withDefault 0 (String.toInt income)
            , scaled = scaledContribution (Maybe.withDefault 0 <| String.toInt income) c.percentage
        }

    else
        c


updatePercentage : Int -> String -> Contribution -> Contribution
updatePercentage id percentage c =
    if c.id == id then
        { c
            | percentage = Maybe.withDefault 0 (String.toInt percentage)
            , scaled = scaledContribution c.income (Maybe.withDefault 0 <| String.toInt percentage)
        }

    else
        c


scaledContribution : Int -> Int -> Int
scaledContribution income percentage =
    round ((toFloat income / toFloat percentage) * 100)



-- VIEW


view : Model -> Html Msg
view model =
    Element.layout
        [ Background.color color.veryLightGray
        , Element.padding 100
        ]
    <|
        Element.column
            [ Element.width Element.fill
            , Element.spacing 40
            , Element.width <| Element.px 500
            , Element.centerX
            ]
            [ title
            , card <| expenseEl model.expense
            , contributionsEl model
            , addEl
            ]


title : Element.Element a
title =
    Element.el [ Element.alignLeft ] <|
        Element.paragraph
            [ Font.size 48
            , Font.variant Font.smallCaps
            ]
            [ Element.text "What is fair?" ]


card : Element.Element Msg -> Element.Element Msg
card content =
    Element.el
        [ Background.color color.white
        , Element.padding 20
        , Border.shadow
            { offset = ( 1, 1 )
            , size = 5
            , blur = 10
            , color = color.gray
            }
        ]
        content


expenseEl : Int -> Element.Element Msg
expenseEl expense =
    Input.text
        [ Background.color color.white
        , Font.alignRight
        ]
        { label = Input.labelAbove [ Font.variant Font.smallCaps ] <| Element.text "Expense"
        , onChange = EditExpense
        , placeholder = Just <| Input.placeholder [] <| Element.text "foo"
        , text = toString expense
        }


contributionsEl : Model -> Element.Element Msg
contributionsEl model =
    Element.column [ Element.spacing 20 ] <|
        List.map card <|
            List.map (contributionEl model) model.contributions


contributionEl : Model -> Contribution -> Element.Element Msg
contributionEl model contribution =
    Element.row
        [ Element.spacing 10
        , Element.inFront <|
            Input.button
                [ Element.width <| Element.px 20
                , Element.height <| Element.px 20
                , Border.rounded 5
                , Element.alignTop
                , Element.alignRight
                , Background.color color.red
                , Element.moveUp 15
                , Element.moveRight 15
                ]
                { onPress = Just <| DeleteContribution contribution.id
                , label =
                    Element.el
                        [ Element.centerY
                        , Element.centerX
                        , Font.size 14
                        , Font.variant Font.smallCaps
                        , Font.color color.white
                        , Font.heavy
                        ]
                    <|
                        Element.text "x"
                }
        ]
        [ Input.text
            [ Element.width <| Element.fillPortion 1
            , Font.alignRight
            ]
            { label = Input.labelAbove [ Font.variant Font.smallCaps ] <| Element.text "Income"
            , onChange = EditIncome contribution.id
            , placeholder = Just <| Input.placeholder [] <| Element.text <| toString contribution.income
            , text = toString contribution.income
            }
        , Input.text
            [ Element.width <| Element.fillPortion 1
            , Font.alignRight
            ]
            { label = Input.labelAbove [ Font.variant Font.smallCaps ] <| Element.text "Percentage"
            , onChange = EditPercentage contribution.id
            , placeholder = Just <| Input.placeholder [] <| Element.text <| toString contribution.percentage
            , text = toString contribution.percentage
            }
        , Element.column
            [ Element.width <| Element.fillPortion 1
            , Element.height Element.fill
            ]
            [ Element.el
                [ Font.variant Font.smallCaps
                , Element.spacing 5
                , Element.alignTop
                ]
              <|
                Element.text "Share"
            , Element.el
                [ Element.padding 13
                , Element.width Element.fill
                , Background.color color.lightGray
                , Border.color color.lightGray
                , Border.rounded 5
                , Font.alignRight
                ]
              <|
                Element.text <|
                    toString <|
                        calculateExpenseContribution model contribution
            ]
        ]


addEl : Element.Element Msg
addEl =
    Input.button
        [ Element.padding 10
        , Background.color color.white
        , Border.width 2
        , Border.color color.gray
        , Border.rounded 5
        ]
        { onPress = Just AddContribution, label = Element.text "+" }


sumContributions : List Contribution -> Int
sumContributions contributions =
    contributions
        |> List.map (\c -> c.scaled)
        |> List.sum


calculateExpenseContribution : Model -> Contribution -> Int
calculateExpenseContribution model contribution =
    if contribution.income /= 0 && contribution.percentage /= 0 then
        round <| toFloat contribution.scaled / toFloat (sumContributions model.contributions) * toFloat model.expense

    else
        0


color =
    { white = Element.rgb255 255 255 255
    , veryLightGray = Element.rgb255 250 250 250
    , lightGray = Element.rgb255 240 240 241
    , gray = Element.rgb255 229 229 230
    , darkGray = Element.rgb255 56 58 66
    , red = Element.rgb255 202 18 67
    , blue = Element.rgb255 64 120 242
    }
